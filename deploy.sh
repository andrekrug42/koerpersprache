echo "# Create Package"
rm koerpersprache.tar.gz
tar -czvf koerpersprache.tar.gz ./html Dockerfile ./build-run.sh

echo "# Send data to server"
scp koerpersprache.tar.gz root@getmob.de:/opt/koerpersprache
scp build-run.sh root@getmob.de:/opt/koerpersprache

echo "# Rund docker build on server and deploy"
ssh root@getmob.de 'cd /opt/koerpersprache; /opt/koerpersprache/build-run.sh'