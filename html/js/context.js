/* HTML Elements */
var elemTextOuput = document.getElementById("text");
var elemSpeakButton = document.getElementById("speak");

var speakStopButton = document.getElementById("speak-stop");
speakStopButton.addEventListener("click", function () { stopHappend = true; });

var triggerButton = document.getElementById("trigger");

var deleteButton = document.getElementById("delete");
deleteButton.addEventListener("click", function () { elemTextOuput.value = "" });

var elemDingDong = document.getElementById("dingdong");

var elemFormName = document.getElementById("form-name");
elemFormName.addEventListener("input", savePerson);

var elemFormDelay = document.getElementById("form-delay");
elemFormDelay.addEventListener("input", savePerson);

//var elemFormSensitifity = document.getElementById("form-sensitifity");
//elemFormSensitifity.addEventListener("input", savePerson);

var elemVideo = document.getElementById("video");
var elemVideoCanvas = document.getElementById("canvas");
var elemMoveCurrent = document.getElementById("count");
var elemMoveNormal = document.getElementById("movmentNormal");
var elemMoveTrigger = document.getElementById("movementTrigger");
var elemMoveLearn = document.getElementById("learnToken");

var elemSentenceInput = document.getElementById("ss-input"); 
var elemSentenceContainer = document.getElementById("ss-container"); 
var elemSentenceTemplate = document.getElementById("ss-template");

var triggerHappend = false;
var stopHappend = false;

var tuningDone = false;
var movementNormal = 0;
var movementTrigger = 0;
var movementCounter = 0;

/* PERSON */
var person = {
    name: "Michael",
    delay: 2,
    senitifity : 50
};

function savePerson() {
    person.delay = elemFormDelay.value;
    person.name = elemFormName.value;
    //person.senitifity = elemFormSensitifity.value;

    localStorage.setItem("person", JSON.stringify([person]));
}

if (localStorage.getItem("person") !== null) {
    var persons = JSON.parse(localStorage.getItem("person"));
    person = persons[0];
    elemFormName.value = person.name;
    elemFormDelay.value = person.delay;
}

function Sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}