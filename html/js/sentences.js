/* SENTENCES */
var sentences = [];

function removeSentence(id) {
    for (var elem of elemSentenceContainer.childNodes) {
        if (elem.id == "s_" + id) {
            elemSentenceContainer.removeChild(elem);
        }
    }
    saveSentences();
    loadSentences();
}

function saveSentences() {
    sentences = [];
    for (var elem of elemSentenceContainer.childNodes) {
        sentences.push(elem.childNodes[3].value);
    }
    localStorage.setItem("sentences", JSON.stringify(sentences));
}

function loadSentences() {
    elemSentenceContainer.innerHTML = "";

    if (localStorage.getItem("sentences") == null) {
        resetSentences();
    } else {
        sentences = JSON.parse(localStorage.getItem("sentences"));
        if (sentences.length == 0) resetSentences();
    }

    sentences = JSON.parse(localStorage.getItem("sentences"));
    elemSentenceContainer.innerHTML = "";
    for (var i = 0; i < sentences.length; i++)  {
        const eId = i;
    
        var eS = elemSentenceTemplate.cloneNode(true);
        eS.style = "display: normal;";
        eS.id = "s_" + i;
        eS.childNodes[1].textContent = eId;
        eS.childNodes[3].value = sentences[i];
        eS.childNodes[3].addEventListener("input", saveSentences);
        eS.childNodes[5].addEventListener("click", function() {removeSentence(eId);});
    
        elemSentenceContainer.append(eS);
    }
}

function addSentence() {
    sentences.push(elemSentenceInput.value);
    localStorage.setItem("sentences", JSON.stringify(sentences));
    loadSentences();
    elemSentenceInput.value = "";
}

function resetSentences() {
    sentences = [];
    sentences.push("Ich muss auf die Toilette");
    sentences.push("Ich habe Hunger");
    sentences.push("Ich habe Durst");
    sentences.push("Mir ist zu kalt");
    sentences.push("Mir ist zu warm");
    sentences.push("Ich habe Schmerzen");
    localStorage.setItem("sentences", JSON.stringify(sentences));
    loadSentences();
}

loadSentences();