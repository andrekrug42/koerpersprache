// Zugriff auf die Web Speech API
var synth = window.speechSynthesis;

async function speakIt(text, tA) {
    triggerHappend = false;

    var finished = false;
    var utterance = new SpeechSynthesisUtterance(text);
    utterance.lang = "de-DE";
    utterance.onend = function () {
        finished = true;
    }

    synth.speak(utterance);

    while (finished !== true) {
        await Sleep(500);
    }

    var dTime = person.delay * 1000;
    while (tA && triggerHappend === false && dTime > 0) {
        dTime -= 100;
        await Sleep(100);
    }

    if (triggerHappend && tA) {
        tA();
        return true;
    }
    return false;
}

/* HELPERS */
var genericSingleChoise = async function (introText, arrayOfOptions) {
    while (true) {
        if (introText !== null) await speakIt(introText);
        for (index in arrayOfOptions) {
            
            //check for end
            if (stopHappend) {
                stopSpeak();
                return;
            }

            var option = arrayOfOptions[index];
            if (await speakIt(option.text, option.func)) return;
            await Sleep(person.delay * 1000);
        }
        await Sleep(person.delay * 1000);
    }
};

function createLetter(l) {
    return {
        text: l,
        func: async function () {
            elemTextOuput.value += l;
            await speakIt("Buchstabe " + l + ", es steht: " + elemTextOuput.value);
            genericSingleChoise(null, lib_abc);
        }
    };
}

function createLetters(letters) {
    lib = [];
    Array.from(letters).forEach(elem => {
        lib.push(createLetter(elem));
    });
    lib.push({
        text: "Zurück", func: async function () {
            genericSingleChoise("Weiter gehts", lib_abc);
        }
    });
    return lib;
}

function createSentence(sentence) {
    return {
        text: sentence, func: async function () {
            elemTextOuput.value += sentence + "\n";
            await speakIt("Alles klar. Ich habe geschrieben: " + sentence);
            genericSingleChoise("Hauptmenü", lib_menu);
        }
    };
}

function getLibSentences() {
    var lS = [];
    sentences.forEach(elem => {
        lS.push(createSentence(elem));
    });
    lS.push({
        text: "Zurück zum Menü", func: async function () {
            genericSingleChoise("Hauptmenü", lib_menu);
        }
    });
    return lS;
}

function getYesNoLib(text, onYes, onNo) {
    var lYN = [];
    lYN.push({
        text: text + ": Ja?!", func: async function () {
            if (onYes) onYes();
        }
    });
    lYN.push({
        text: text + ": Nein?!", func: async function () {
            if (onNo) onNo();
        }
    });
    return lYN;
}

async function stopSpeak() {
    await speakIt("Auf Wiedersehen, " + person.name);

    speakStopButton.style = "display: none;";
    elemSpeakButton.style= "display: normal;";
}

/* HAUPTMENÜ */
var lib_menu = [{
    text: "Standardsätze auswählen", func: async function () {
        genericSingleChoise("Okay, Standardsätze, folgende stehen zur Auswahl:", getLibSentences());
    }
}, {
    text: "Buchstabieren", func: async function () {
        genericSingleChoise("Okay, dann buchstabieren wir:", lib_abc);
    }
}, {
    text: "Klingelton abspielen", func: async function () {
        elemDingDong.play();
        await Sleep(2000);
        genericSingleChoise("Hauptmenü", lib_menu);
    }
}, {
    text: "Vorlesen was du geschrieben hast", func: async function () {
        await speakIt("Es steht geschrieben: " + elemTextOuput.value);
        genericSingleChoise("Hauptmenü", lib_menu);
    }
}, {
    text: "Deinen geschriebenen Text löschen", func: async function () {

        var yNL = getYesNoLib("Bist du dir sicher dass du den Text " + elemTextOuput.value + " löschen möchtest?",
        async function() {
            await speakIt("Text gelöscht");
            elemTextOuput.value = "";
            genericSingleChoise("Hauptmenü", lib_menu);
        }, async function() {
            await speakIt("Text nicht gelöscht");
            genericSingleChoise("Hauptmenü", lib_menu);
        });

        genericSingleChoise("Hauptmenü", yNL);
    }
},  {
    text: "Programm beenden", func: async function () {        
        stopSpeak();
    }
}];

/* ABC */
var lib_a_d = createLetters("abcd");
var lib_e_h = createLetters("efgh");
var lib_i_m = createLetters("ijklm");
var lib_n_q = createLetters("nopq");
var lib_r_z = createLetters("rstuvwxyz");
var lib_zahlen = createLetters("1234567890");

var lib_abc = [{
    text: "A B C D", func: async function () {
        genericSingleChoise("OK, A bis D", lib_a_d);
    }
}, {
    text: "E F G H", func: async function () {
        genericSingleChoise("OK, E bis H", lib_e_h);
    }
}, {
    text: "I J K L M", func: async function () {
        genericSingleChoise("OK, I bis M", lib_i_m);
    }
}, {
    text: "N O P Q", func: async function () {
        genericSingleChoise("OK, N bis Q", lib_n_q);
    }
}, {
    text: "R bis Z", func: async function () {
        genericSingleChoise("OK, R bis Z", lib_r_z);
    }
}, {
    text: "Zahlen", func: async function () {
        genericSingleChoise("OK, Zahlen", lib_zahlen);
    }
}, {
    text: "Leerzeichen", func: async function () {
        if (elemTextOuput.value == "" || elemTextOuput.value.slice(-1) != " ") {
            elemTextOuput.value += " ";
            await speakIt("Okay, Leerzeichen hinzugefügt. Es steht: " + elemTextOuput.value);
        } else {
            await speakIt("Ein Leerzeichen macht gerade keinen Sinn. Es steht: " + elemTextOuput.value);
        }

        genericSingleChoise("Weiter gehts", lib_abc);
    }
}, {
    text: "Letzter Buchstabe löschen", func: async function () {
        elemTextOuput.value = elemTextOuput.value.slice(0, -1);
        await speakIt("Gelöscht, es steht: " + elemTextOuput.value);
        genericSingleChoise(null, lib_abc);
    }
}, {
    text: "Buchstabieren beenden", func: async function () {
        elemTextOuput.value += "\n";
        await speakIt("Okay, Du hast geschrieben: " + elemTextOuput.value + ". Ich lasse den Text so stehen.");
        genericSingleChoise("Hauptmenü", lib_menu);
    }
}];

async function tuning() {
    await speakIt("Bevor wir starten, müssen wir die Bewegungserkennung prüfen und richtig einstellen. Bitte Bewege dich so, wie du dich zum Melden bewegen möchtest");

    person.senitifity = 100;

    while (tuningDone === false) {
        if (stopHappend) {
            stopSpeak();
            return;
        }
        //await Sleep(500);

        if (movementCounter === 0) {
            await speakIt("Gerade kann ich nicht auf deine Kamera zugreifen. Bitte Kamerazugriff prüfen und einrichten.");
            stopSpeak();
            return;
        }

        if (movementCounter % 25 == 0) speakIt("Bitte Bewege dich so, wie du dich zum Melden bewegen möchtest");

        await Sleep(500);

        if (movementTrigger > 4500 && movementTrigger < 5500) {
            tuningDone = true;
        } else if (movementTrigger < 4500) {
            person.senitifity--;
        } else  if (movementTrigger > 5500) {
            person.senitifity++;
        }
    }
    await speakIt("Gut gemacht, "+ person.name +", wir sind bereit, los geht's!");

}

async function startConversation() {
    stopHappend = false;
    triggerHappend = false;
    movementCounter = 0;

    elemSpeakButton.style = "display: none;";
    speakStopButton.style= "display: normal;";

    await speakIt("hallo " + person.name + ", dann legen wir mal los.");

    await tuning();

    genericSingleChoise("Hauptmenü, was möchtest du tun?", lib_menu);
}

async function startIntroduction() {
    await speakIt("Hallo "+ person.name +". Dieses Programm hilft dir dabei, dich mit anderen zu unterhalten und dich auszudrücken. Du brauchst dafür eine Kamera, die dich sehen kann. Wenn du dich bewegst, kann die Software das erkennen. Dann kannst du aus verschiedenen Optionen wählen. Zum Beispiel kannst du dir Sätze anhören, die du oft brauchst. Wenn du einen Satz auswählen möchtest, musst du dich einmal bewegen. Dann wird der Satz auf dem Bildschirm gezeigt. So können andere verstehen, was du sagen willst. Du kannst auch deine eigenen Sätze schreiben, indem du Buchstaben auswählst. Dieses Programm ist sehr sicher und privat. Sie schickt keine Daten an andere oder speichert sie. Sie hat auch keine Werbung, keine Kosten, keine Anmeldung und keine anderen Verpflichtungen. Du kannst das Programm einfach benutzen, wann immer du willst.");
}

var triggerTimeout = false;
var trigger = function () {

    console.debug("call trigger with timeout ", triggerTimeout, triggerTimeout === false);
    
    if (triggerTimeout === false) {
        triggerTimeout = true;
        triggerHappend = true;
        setTimeout(function() {
            triggerTimeout = false;
        }, person.delay*1000);
        console.debug("fire");
    } else {
        console.debug("no fire");
    }
}

// Füge einen Event-Listener hinzu, der die speak-Funktion aufruft, wenn der Button geklickt wird
elemSpeakButton.addEventListener("click", startConversation);