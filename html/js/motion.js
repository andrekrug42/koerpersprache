var media = navigator.mediaDevices;
var context = elemVideoCanvas.getContext("2d");
var previous = null;
var threshold = 100;

// Eine Funktion, die das Video von der Webcam anfordert
function requestVideo() {
    // Fordere den Zugriff auf die Webcam an
    media.getUserMedia({ video: true })
        .then(function (stream) {
            elemVideo.srcObject = stream;
            elemVideo.addEventListener("canplay", detectMotion);
        })
        .catch(function (error) {
            document.getElementById("technical_cam_error").style = "";
            document.getElementById("technical_cam_error_reason").innerText = error.name + " " + error.message;
        });
}

function detectMotion() {
    movementCounter++; 
    elemMoveLearn.textContent = person.senitifity;

    // Zeichne das aktuelle Bild vom Video auf den Canvas
    context.drawImage(elemVideo, 0, 0, elemVideoCanvas.width, elemVideoCanvas.height);

    var current = context.getImageData(0, 0, elemVideoCanvas.width, elemVideoCanvas.height);
    var moving = 0;

    if (previous) {
        for (var i = 0; i < current.data.length; i += 4) {
            // Berechne die Helligkeit des aktuellen Pixels
            var brightnessCurrent = (current.data[i] + current.data[i + 1] + current.data[i + 2]) / 3;
            // Berechne die Helligkeit des vorherigen Pixels
            var brightnessPrevious = (previous.data[i] + previous.data[i + 1] + previous.data[i + 2]) / 3;
            // Berechne den Unterschied zwischen den Helligkeiten
            var difference = Math.abs(brightnessCurrent - brightnessPrevious);
            // Wenn der Unterschied größer als die Schwelle ist, zähle es als ein sich bewegendes Pixel
            if (difference > person.senitifity) {
                moving++;
            }
        }
    }

    // Speichere das aktuelle Bild als das vorherige Bild für den nächsten Durchlauf
    previous = current;

    if (movementNormal === 0) {
        movementNormal = moving;
    } else if (tuningDone === false) {
        movementNormal = Math.round(((movementNormal * 9) + moving) / 10);
        movementTrigger = movementNormal * 3;
    }
    
    elemMoveCurrent.textContent = moving;
    elemMoveNormal.textContent = movementNormal;
    elemMoveTrigger.textContent = movementTrigger;

    if (moving > movementTrigger) {
        trigger();
    }

    // Wiederhole die Bewegungserkennung mit einer Verzögerung von 100 Millisekunden
    setTimeout(detectMotion, 200);
}

// Rufe die requestVideo-Funktion auf, wenn die Seite geladen wird
window.addEventListener("load", requestVideo);