echo "# Extract tar archive"
tar -xf koerpersprache.tar.gz

docker build -t koerpersprache .

echo "# docker stop, rm, run, start"
docker stop koerpersprache
docker rm koerpersprache
docker run \
	--name koerpersprache \
	-v /etc/localtime:/etc/localtime:ro \
	-v /etc/timezone:/etc/timezone:ro \
	-d \
    -p 127.0.0.1:8080:80 \
	--restart=always \
	koerpersprache

docker start koerpersprache